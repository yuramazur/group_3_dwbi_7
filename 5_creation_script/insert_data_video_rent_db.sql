use [video_rent]

go
-----------------------------------------------------------------------------------------------------------------
------------------------------------- INSERT INTO VALUES area  --------------------------------------------------
-- Orest Malanchuk --/ begin / insert data(values) for : [employee] 

--table  employee
INSERT INTO employee (department_id,position_id,bonus,boss_id,first_name,middle_name,last_name,identity_id,contact_id,[user_id])
VALUES (1,1,1000,NULL,'Francis','Joseph','George ',1,1,1),
(3,1,1000,1,'Philip','Peter','Whitehead',2,2,2),
(1,1,1000,1,'Damian','George','Brooks',3,3,3),
(3,2,250,2,'Christopher','Walter','Norton',4,4,4),
(1,2,250,2,'James','Dwain','Clark',5,5,5),
(1,2,250,2,'Warren','Donald','Sherman',6,6,6),
(3,3,250,2,'Jeremy','Ethan','Allison',7,7,7),
(2,3,250,2,'William�','Melvyn','Gilbert',8,8,8),
(1,3,250,2,'Edward','Myles','Quinn',9,9,9),
(4,3,250,2,'Joseph','James','Curtis',10,10,10)

GO
-- table [identity]
INSERT INTO [dbo].[identity] (identity_name, serial_number)
VALUES('passport','KS10682'),
('passport','KS18298'),
('passport','KS25914'),
('passport','KS33530'),
('student id','ST41146'),
('student id','ST48762'),
('student id','ST56378'),
('student id','ST63994'),
('student id','ST71610'),
('driver license','DR79226'),
('driver license','DR86842'),
('driver license','DR94458'),
('passport','KS10010'),
('passport','KS20187'),
('passport','KS30364'),
('passport','KS40541'),
('student id','ST50718'),
('student id','ST60895'),
('student id','ST71072'),
('student id','ST81249'),
('student id','ST91426'),
('driver license','DR10160'),
('driver license','DR11178'),
('driver license','DR12195')
GO
-- table [[location]]

INSERT INTO dbo.[location](country_id, region_id , district_id , city_id , street_id )
VALUES  (1,NULL,NULL,1,NULL),
		(1,NULL,NULL,2,NULL),
		(2,NULL,NULL,3,NULL),
		(2,NULL,NULL,4,NULL),
		(3,NULL,NULL,5,NULL),
		(3,NULL,NULL,6,NULL),
		(4,NULL,NULL,7,NULL),
		(4,NULL,NULL,8,NULL),
		(5,NULL,NULL,9,NULL),
		(5,NULL,NULL,10,NULL)
		
-- Orest Malanchuk --/ the end / insert data(values) for : [employee]
-----------------------------------------------------------------------------------------------------------------
-- Yura Mazur --/ BEGIN /  ---------------------------------------------------------------------------------

GO
-- TABLE CURRENCY
INSERT INTO [dbo].[currency]([currency_name]) VALUES ('USD'),
							                         ('UAN'),
							                         ('EUR'),
							                         ('GBR'),
							                         ('OMR'),
							                         ('BND'),
							                         ('AUD'),
							                         ('CHF'),
							                         ('BHD'),
							                         ('KWD')
GO
-- TABLE CONTACT
INSERT INTO [dbo].[contact] ([e_mail],[ICQ],[skype]) VALUES ('one@gmain.com','1111','one'),
                                                            ('two@gmain.com','2222','two'),
															('three@gmain.com','3333','three'),
															('four@gmain.com','4444','four'),
															('five@gmain.com','5555','five'),
															('six@gmain.com','6666','six'),
															('seven@gmain.com','7777','seven'),
															('eight@gmain.com','8888','eighn'),
															('nine@gmain.com','9999','nine'),
															('ten@gmain.com','10101010','ten'),
															('eleven@gmain.com','11111111',''),
															('tvelve@gmain.com','12121212','tvelve'),
                                                            ('13@gmain.com','13131313','13'),
															('14@gmain.com','14141414','14'),
															('15@gmain.com','15151515','15'),
															('16@gmain.com','16161616','16'),
															('17@gmain.com','17171717','17'),
															('18@gmain.com','18181818','18'),
															('19@gmain.com','19191919','19'),
															('20@gmain.com',NULL,'20'),
															('21@gmain.com',NULL,'21'),
															('22@gmain.com',NULL,'22')
GO
-- TABLE CATEGORY 
INSERT INTO [dbo].[category]([category_name]) VALUES ('HORROR'),
                                                     ('THRILLER'),
													 ('COMEDY'),
													 ('DRAMA'),
													 ('ACTION'),
													 ('FANTASY'),
													 ('MELODRAMA'),
													 ('DETECTIVE'),
													 ('CARTOONS'),
													 ('ANIME')

GO
-- TABLE DISTRICT
INSERT INTO [dbo].[district]([district_name]) VALUES ('Sokalski'),
                                                     ('Jovkivski'),
													 ('Brodivski'),
													 ('Drogobichski'),
													 ('Zolochivski'),
													 ('Radehivski'),
													 ('Skolivski'),
													 ('Turkivski'),
													 ('Mikolaivski'),
													 ('Buski')
                
GO
-- TABLE CITY
INSERT INTO [dbo].[city]([city_name], [zip_code] ) VALUES ('LA','0000000'),
                                                          ('NY','1111111'),
														  ('KIEV','333333'),
														  ('LVIV','444444'),
														  ('KRAKIV','555555'),
														  ('WARSAW','666666'),
														  ('KOPENGAGEN','777777'),
														  ('ODENSE','888888'),
														  ('PARIS','999999'),
														  ('MARSEL','1212221')
 
GO
-- TABLE COUNTRY
INSERT INTO [dbo].[country]([country_name]) VALUES        ('USA'),
                                                          ('UKRAINE'),
														  ('POLAND'),
														  ('DENMARK'),
														  ('FRANCE'),
														  ('UK')
												  
GO
--TABLE CONTACT_PHONE
INSERT INTO [dbo].[contact_phone]([contact_id],[phone_id]) VALUES (1,1),
                                                                  (2,2),
																  (3,3),
																  (4,4),
																  (5,5),
																  (6,6),
																  (7,7),
																  (8,8),
																  (9,9),
																  (10,10)

GO
-- TABLE ACTOR
INSERT INTO [dbo].[actor]([first_name],[middle_name],[last_name],[country_id]) VALUES ('Bogdan',NULL,'Stupka',2),
                                                                                      ('Mihal',NULL,'Gembrovski',3),
																					  ('Sylvester',NULL,'Stallone',1),
																					  ('Arnold',NULL,'Schwarzenegger',1),
																					  ('Leonardo',NULL,'DiCaprio',1),
																					  ('Brad',NULL,'Pitt',1),
																					  ('Angelina',NULL,'Jolie',1),
																					  ('Elijah',NULL,'Wood',1),
																					  ('Scarlett',NULL,'Johansson',1),
																					  ('Ewan','Gordon','McGregor',6)

GO
--TABLE CLIENT
INSERT INTO [dbo].[client]([status_id],[first_name],[middle_name],[last_name],[identity_id],[contact_id],[location_id])
                                            VALUES (1,'Pavlo',NULL,'Pavlov',10,5,4),
											       (2,'Antony',NULL,'Hopkins',11,1,1),
												   (1,'Jhon',NULL,'Dow',12,2,2),
												   (2,'Igor',NULL,'Peleh',13,3,3),
												   (2,'Marek',NULL,'Jadowski',14,4,5)
GO
-- TABLE DEPARTMENT
INSERT INTO [dbo].[department]([department_name],[location_id]) values ('1',4),
                                                                       ('2',1),
																	   ('3',2),
																	   ('4',3),
																	   ('5',5),
																	   ('6',6),
																	   ('7',7),
																	   ('8',8),
																	   ('9',9),
																	   ('10',10)

GO

-- Yura Mazur --/ END /  -----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-- Vasylyna Kharkhalis --/ BEGIN /  ---------------------------------------------------------------------------------

GO
-- TABLE MOVIE
INSERT INTO [dbo].[movie]([movie_name],[movie_duraction]) VALUES ('Forrest Gump',02:22:00,1994,8.8,'The presidencies of Kennedy and Johnson unfold throufh the perspective of an Alabama man'),
							                                     ('A Beautiful Mind',02:15:00,2001,8.2,'About John Nash a brilliant but asocial mathematician'),
							                                     ('Green Mile',03:09:00,1999,8.5,'The lives of guards on Death Row are affected by one of their charges'),
							                                     ('The Terminal',02:08:00,2004,7.3,'An eastern immigrant finds himself stranded in JFK airport'),
							                                     ('500 Days of Summer',01:35:00,2009,7.7,'Romantic comedy about a women who doesnt believe in true love')
							                                     ('Saving Private Ryan',02:49:00,1998,8.6,'A group of U.S. soldiers go behind enemy lines to retrieve a paratrooper'),
							                                     ('Braveheart',02:58:00,1995,8.4,'Sir William Wallace begins a revolt against King Edward I of England'),
							                                     ('The Matrix',02:16:00,1999,8.7,'A computer hacker learns from mysterious rebels about the true nature of his reality'),
							                                     ('Gladiator',02:35:00,2000,8.5,'When a Roman General is betrayed, and his family murdered, he comes as a gladiator'),
							                                     ('The Pianist',02:18:00,2002,8.5,'A Polish Jewish musician struggles to survive the destruction of the Warsaw')
GO
-- TABLE MOVIE ACTOR
INSERT INTO [dbo].[movie_actor] ([movie_id],[actor_id]) VALUES (3,7),
                                                               (2,5),
															   (8,10),
															   (1,6),
															   (9,1),
															   (2,2),
															   (10,3),
															   (7,7),
															   (4,5),
														       (6,8)
GO
-- TABLE MOVIE CATEGORY 
INSERT INTO [dbo].[movie_category]([movie_id],[category_id]) VALUES (9,9),
                                                                    (10,5),
															        (8,10),
															        (1,9),
															        (1,1),
															        (2,2),
															        (3,3),
															        (7,4),
															        (5,5),
														            (6,6)
GO
-- TABLE MOVIE COUNTRY
INSERT INTO [dbo].[movie_country]([movie_id],[country_id]) VALUES (7,9),
                                                                  (10,8),
															      (9,10),
															      (1,10),
															      (1,1),
															      (2,2),
															      (3,3),
															      (7,4),
															      (5,5),
														          (6,6)             
GO
-- TABLE MOVIE PRODUCER
INSERT INTO [dbo].[movie_producer]([movie_id],[producer_id] ) VALUES (7,7),
                                                                     (9,8),
															         (9,1),
															         (3,10),
															         (1,5),
															         (6,2),
															         (3,8),
															         (10,4),
															         (5,2),
														             (4,6)  
GO
-- TABLE MOVIE STUDIO
INSERT INTO [dbo].[movie_studio]([movie_id],[studio_id]) VALUES  (7,5),
                                                                 (6,8),
															     (9,7),
															     (8,10),
															     (1,9),
															     (10,2),
															     (3,1),
															     (2,4),
															     (5,3),
														         (4,6)  
GO
--TABLE ORDER
INSERT INTO [dbo].[order]([department_id],[order_type_id],[employee_id],[client_id],[total_price],[currency_id],[payment_id]) VALUES
                          (2,2,1,2,50,3,4),
						  (5,2,1,6,50,3,4),
						  (2,7,1,2,80,3,4),
						  (2,2,9,2,50,3,10),
						  (1,2,1,2,50,3,4),
						  (2,2,1,2,30,3,4),
						  (2,4,1,2,50,5,4),
		    		      (2,2,6,2,50,3,7),
					      (8,2,1,2,50,3,4),
						  (2,2,1,2,90,3,4)
GO
-- Vasylyna Kharkhalis --/ END /  -----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-- Khrystyna Mykich --/ BEGIN /  ---------------------------------------------------------------------------------

-- TABLE street
INSERT INTO [dbo].[street]([street_name])
VALUES  ('Forest_Hills'),
		('Bandera'),
		('Shevchenko'),
		('Katowice'),
		('West_Main'),
		('Willow'),
		('Franklin'),
		('Lee'),
		('Madison'),
		('Central')
GO

-- TABLE studio
INSERT INTO [dbo].[studio]([studio_name])
VALUES  ('Vegas'),
		('Sony_Vegas'),
		('Universal_Pictures'),
		('Walt_Disney_Pictures'),
		('20th_Century_Fox'),
		('Warner_Bros.Pictures'),
		('Paramount_Pictures'),
		('STXfilms'),
		('Lionsgate_Films'),
		('Broad_Green_Pictures')
GO

-- TABLE tape_manufacturer
INSERT INTO [dbo].[tape_manufacturer]([manufacturer_name], [country_id])
VALUES  ('Last_Audio', 6),
		('TAKT', 3),
		('Golding_Replication', 6),
		('VHS_vs._Betamax', 1),
		('National_Audio', 1)
	
GO

-- TABLE user
INSERT INTO [dbo].[user]([login], [password],[role_id])
VALUES  ('user1', '11111qwe', 1),
		('user2', '12376fjg', 1),
		('user3', '56748haa', 4),
		('user4', '98750ppp', 3),
		('user5', '04111laq', 3),
		('user6', '2431739o', 2),
		('user7', '765342oi', 1),
		('user8', '765fgt5d', 2),
		('user9', '76543grt', 5),
		('user10', '761111lo', 2)
GO

-- TABLE user_role
INSERT INTO [dbo].[user_role]([role_name])
VALUES  ('Sysadmin'),
		('Serveradm'),
		('Dbcreator'),
		('Db_owner'),
		('Datareader')
GO

-- TABLE [tape_type]
INSERT INTO [dbo].[tape_type]([type_name])
VALUES  ('VHS'),
		('S-VHS'),
		('VHS-�'),
		('Video8'),
		('D-9')
GO

-- TABLE video_tape
INSERT INTO [dbo].[video_tape]([type_id], [manufacturer_id], [movie_id])
VALUES  (1, 1, 3),
		(3, 2, 1),
		(2, 1, 4),
		(1, 3, 1),
		(4, 1, 2),
		(5, 4, 2),
		(1, 4, 5),
		(4, 2, 7),
		(5, 3, 8),
		(2, 2, 6)
GO

-- TABLE video_tape_oreder
INSERT INTO [dbo].[video_tape_order]([order_id], [video_tape_id], [count])
VALUES  (1, 1, 2),
		(2, 3, 1),
		(3, 2, 3),
		(4, 6, default),
		(5, 9, 1),
		(6, 4, 2),
		(7, 10, 3),
		(8, 8, 3),
		(9, 7, 2),
		(10, 5, default)
GO

-- TABLE video_type_price
INSERT INTO [dbo].[video_type_price]([order_type_id],[department_id], [video_tape_id], [price], [currency_id])
VALUES  (1, 4, 4, 2, 1),
		(2, 2, 3, 100, 2),
		(1, 6, 5, 8, 1),
		(2, 1, 5, 95, 4),
		(1, 3, 7, 23, 6),
		(2, 1, 2, 13, 8),
		(1, 5, 8, 4, 1),
		(2, 4, 6, 110, 2),
		(1, 2, 1, 13, 7),
		(2, 7, 1, 97, 2)
GO	
-- Khrystyna Mykich --/ END /  ---------------------------------------------------------------------------------
										      