use [master]
go
drop database if exists [video_rent]

create database [video_rent]

use [video_rent]

go
------------------------------------- table creation area ------------------------------------------------------
-- Orest Malanchuk  / begin /-- table: [employee] , [location] , [identity]
CREATE TABLE [dbo].[employee]
(
 [id]            BIGINT IDENTITY (1, 1) NOT NULL ,
 [department_id] BIGINT NOT NULL ,
 [position_id]   INT NOT NULL ,
 [bonus]         MONEY NULL ,
 [boss_id]       BIGINT NULL ,
 [first_name]    VARCHAR(50) NOT NULL ,
 [middle_name]   VARCHAR(50) NULL ,
 [last_name]     VARCHAR(50) NOT NULL ,
 [identity_id]   TINYINT NOT NULL ,
 [contact_id]    BIGINT NOT NULL ,
 [user_id]       BIGINT NOT NULL ,

 CONSTRAINT [PK_employee] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_user_id] UNIQUE NONCLUSTERED ([user_id] ASC),
 );
 GO

CREATE TABLE [dbo].[location]
(
 [id]          BIGINT IDENTITY (1, 1) NOT NULL  ,
 [country_id]  INT NOT NULL ,
 [region_id]   BIGINT NULL ,
 [district_id] BIGINT NULL ,
 [city_id]     BIGINT NOT NULL ,
 [street_id]   BIGINT NULL ,

 CONSTRAINT [PK_location] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_location] UNIQUE NONCLUSTERED ([country_id] ASC, [region_id] ASC, [district_id] ASC, [city_id] ASC),
);
GO

CREATE TABLE [dbo].[identity]
(
 [id]            TINYINT IDENTITY (1, 1) NOT NULL ,
 [identity_name] VARCHAR(50) NOT NULL ,
 [serial_number] VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_identity] PRIMARY KEY CLUSTERED ([id] ASC ),
 
);
GO
-- Orest Malanchuk  / the end /-- table: [employee] , [location] , [identity]

-- Yura Mazur  / BEGIN /-- table: [actor] , [category] , [city], [client],[contact],[contact_phone],[country],[currency],[department],[district]

CREATE TABLE [dbo].[currency]
(
 [id]            TINYINT IDENTITY (1,1)  NOT NULL ,
 [currency_name] VARCHAR(4) NOT NULL ,

 CONSTRAINT [pk_currency] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_currency] UNIQUE NONCLUSTERED ([currency_name] ASC)
);
GO

CREATE TABLE [dbo].[contact]
(
 [id]     BIGINT IDENTITY (1, 1) NOT NULL ,
 [e_mail] VARCHAR(50) NULL ,
 [ICQ]    VARCHAR(50) NULL ,
 [skype]  VARCHAR(50) NULL ,

 CONSTRAINT [pk_contact] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_e_mail] UNIQUE NONCLUSTERED ([e_mail] ASC),
 CONSTRAINT [ak_ISQ] UNIQUE NONCLUSTERED ([ICQ] ASC),
 CONSTRAINT [ak_skype] UNIQUE NONCLUSTERED ([skype] ASC)
);
GO

CREATE TABLE [dbo].[category]
(
 [id]            INT IDENTITY (1, 1) NOT NULL ,
 [category_name] VARCHAR(30) NOT NULL ,

 CONSTRAINT [pk_category] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_category] UNIQUE NONCLUSTERED ([category_name] ASC)
);
GO

CREATE TABLE [dbo].[district]
(
 [id]            BIGINT IDENTITY (1, 1) NOT NULL ,
 [district_name] VARCHAR(50) NOT NULL ,

 CONSTRAINT [pk_district] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_district_name_region_id] UNIQUE NONCLUSTERED ([district_name] ASC)
);
GO

CREATE TABLE [dbo].[city]
(
 [id]        BIGINT IDENTITY (1, 1) NOT NULL ,
 [city_name] VARCHAR(50) NOT NULL ,
 [zip_code]  VARCHAR(50) NOT NULL ,

 CONSTRAINT [pk_city] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_city_name_district_id_region_id_country_id] UNIQUE NONCLUSTERED ([city_name] ASC)
);
GO

CREATE TABLE [dbo].[country]
(
 [id]           INT IDENTITY (1, 1) NOT NULL ,
 [country_name] VARCHAR(30) NOT NULL ,

 CONSTRAINT [pk_country] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_country_name] UNIQUE NONCLUSTERED ([country_name] ASC)
);
GO

CREATE TABLE [dbo].[contact_phone]
(
 [contact_id] BIGINT NOT NULL ,
 [phone_id]   BIGINT NOT NULL ,

 CONSTRAINT [ak_contact_phone] UNIQUE NONCLUSTERED ([contact_id] ASC, [phone_id] ASC)
 );
GO

CREATE TABLE [dbo].[actor]
(
 [id]          INT IDENTITY (1, 1) NOT NULL ,
 [first_name]  VARCHAR(50) NOT NULL ,
 [middle_name] VARCHAR(50) NOT NULL ,
 [last_name]   VARCHAR(50) NOT NULL ,
 [country_id]  INT NOT NULL ,

 CONSTRAINT [pk_actor] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_actro] UNIQUE NONCLUSTERED([first_name], [middle_name], [last_name])
 );
GO

CREATE TABLE [dbo].[client]
(
 [id]          BIGINT IDENTITY (1, 1) NOT NULL ,
 [status_id]   TINYINT NOT NULL ,
 [first_name]  VARCHAR(50) NOT NULL ,
 [middle_name] VARCHAR(50) NULL ,
 [last_name]   VARCHAR(50) NOT NULL ,
 [identity_id] TINYINT NULL ,
 [contact_id]  BIGINT NOT NULL ,
 [location_id] BIGINT NOT NULL ,

 CONSTRAINT [pk_client] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_contact] UNIQUE NONCLUSTERED ([contact_id] ASC),
 );
GO

CREATE TABLE [dbo].[department]
(
 [id]              BIGINT IDENTITY (1, 1) NOT NULL ,
 [department_name] VARCHAR(50) NOT NULL ,
 [location_id]     BIGINT NOT NULL ,

 CONSTRAINT [PK_department] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_department] UNIQUE NONCLUSTERED ([department_name] ASC, [location_id] ASC),
 );
GO
-- Yura Mazur  / END /-- table: [actor] , [category] , [city], [client],[contact],[contact_phone],[country],[currency],[department],[district]

-- Vasylyna Kharkhalis  / BEGIN /-- table: [movie] , [movie_actor] , [movie_category], [movie_country],[movie_producer],[movie_studio],[order]

CREATE TABLE [dbo].[movie]
(
 [id]             BIGINT IDENTITY (1,1)  NOT NULL ,
 [movie_name]     VARCHAR(140)   NOT NULL ,
 [movie_duration] TIME(7)        NOT NULL,
 [movie_year]     SMALLINT       NOT NULL,
 [movie_rating]   DECIMAL(4,2)   NOT NULL,
 [movie_description] VARCHAR(1000)  NOT NULL,

 CONSTRAINT [pk_movie] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO

CREATE TABLE [dbo].[movie_actor]
(
 [movie_id]     BIGINT NOT NULL ,
 [actor_id]     INT    NOT NULL 
);
GO

CREATE TABLE [dbo].[movie_category]
(
 [movie_id]    BIGINT NOT NULL ,
 [category_id] INT    NOT NULL 

);
GO

CREATE TABLE [dbo].[movie_country]
(
 [movie_id]     BIGINT NOT NULL ,
 [country_id]   INT    NOT NULL
);
GO

CREATE TABLE [dbo].[movie_producer]
(
 [movie_id]     BIGINT NOT NULL ,
 [producer_id]  INT    NOT NULL 
 
);
GO

CREATE TABLE [dbo].[movie_studio]
(
 [movie_id]  BIGINT NOT NULL ,
 [studio_id] INT    NOT NULL 
 
);
GO

CREATE TABLE [dbo].[order]
(
 [id]            BIGINT IDENTITY (1,1)  NOT NULL ,
 [order_date]    DATETIME NOT NULL CONSTRAINT [DF_order_order_date] DEFAULT getdate() ,
 [department_id] BIGINT   NOT NULL ,
 [order_type_id] TINYINT  NOT NULL,
 [employee_id]   BIGINT   NOT NULL,
 [client_id]     BIGINT   NOT NULL,
 [total_price]   MONEY    NOT NULL,
 [currency_id]   TINYINT  NOT NULL,
 [payment_id]    TINYINT  NOT NULL,

  CONSTRAINT [pk_order] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO

-- Vasylyna Kharkhalis  / END /-- table: [movie] , [movie_actor] , [movie_category], [movie_country],[movie_producer],[movie_studio],[order]

-- Khrystyna Mykich / begin /-- table: [street], [studio], [tape_manufacturer], [user], [user_role],
-- [video_tape], [video_tape_order], [video_type_price], [tape_type]


CREATE TABLE [dbo].[street]
(
 [id]          BIGINT IDENTITY (1, 1) NOT NULL ,
 [street_name] VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_street] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_street] UNIQUE NONCLUSTERED ([street_name] ASC)
)
GO

CREATE TABLE [dbo].[studio]
(
 [id]          INT IDENTITY (1, 1) NOT NULL ,
 [studio_name] VARCHAR(60) NOT NULL,
 
 CONSTRAINT [pk_studio] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [UQ__studio__ABA8EFE473906BA9] UNIQUE NONCLUSTERED ([studio_name] ASC)
)
GO

CREATE TABLE [dbo].[tape_manufacturer]
(
 [id]                SMALLINT IDENTITY (1, 1) NOT NULL ,
 [manufacturer_name] VARCHAR(20) NOT NULL ,
 [country_id]        INT NOT NULL ,

 CONSTRAINT [PK_manufacturer] PRIMARY KEY CLUSTERED ([id] ASC)
 
)
GO

CREATE TABLE [dbo].[user]
(
 [id]       BIGINT IDENTITY (1, 1) NOT NULL ,
 [login]    VARCHAR(20) NOT NULL ,
 [password] VARCHAR(30) NOT NULL ,
 [role_id]  TINYINT NOT NULL ,

 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_user] UNIQUE NONCLUSTERED ([login] ASC)

)
GO

CREATE TABLE [dbo].[user_role]
(
 [id]        TINYINT IDENTITY (1, 1) NOT NULL ,
 [role_name] VARCHAR(10) NOT NULL ,

 CONSTRAINT [PK_user_role] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_user_role] UNIQUE NONCLUSTERED ([role_name] ASC)
)
GO

CREATE TABLE [dbo].[video_tape]
(
 [id]              BIGINT IDENTITY (1, 1) NOT NULL ,
 [type_id]         TINYINT NOT NULL ,
 [manufacturer_id] SMALLINT NOT NULL ,
 [movie_id]        BIGINT NOT NULL ,

 CONSTRAINT [PK_video_tape] PRIMARY KEY CLUSTERED ([id] ASC)
 )
GO

CREATE TABLE [dbo].[video_tape_order]
(
 [order_id]      BIGINT NOT NULL ,
 [video_tape_id] BIGINT NOT NULL ,
 [count]         TINYINT NOT NULL CONSTRAINT [DF_video_tape_order_count] DEFAULT 1 
)

GO

CREATE TABLE [dbo].[video_type_price]
(
 [order_type_id] TINYINT NOT NULL ,
 [department_id] BIGINT NOT NULL ,
 [video_tape_id] BIGINT NOT NULL ,
 [price]         MONEY NOT NULL ,
 [currency_id]   TINYINT NOT NULL ,

 CONSTRAINT [ak_operation_price] UNIQUE NONCLUSTERED
([currency_id] ASC, [department_id] ASC, [order_type_id] ASC, [price] ASC, [video_tape_id] ASC)
)

GO

CREATE TABLE [dbo].[tape_type]
(
 [id]        TINYINT IDENTITY (1, 1) NOT NULL ,
 [type_name] VARCHAR(10) NOT NULL ,

 CONSTRAINT [PK_tape_type] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [ak_tape_type] UNIQUE NONCLUSTERED ([type_name] ASC)
);
GO

-----------------------------------------------------------------------------------------------------------------
------------------------------------- foreign key creation area  ------------------------------------------------
-- Orest Malanchuk --/ begin / relationship for : [employee] , [location] 


ALTER TABLE [dbo].[location]  
ADD
CONSTRAINT [fk_location_country] FOREIGN KEY ([country_id])
REFERENCES [dbo].[country]([id]) ON DELETE CASCADE  GO

ALTER TABLE [dbo].[location]  
ADD
CONSTRAINT [fk_location_region] FOREIGN KEY ([region_id])
REFERENCES [dbo].[region]([id]) ON DELETE CASCADE  GO

ALTER TABLE [dbo].[location] 
ADD CONSTRAINT [fk_location_district] FOREIGN KEY ([district_id])
REFERENCES [dbo].[district]([id]) ON DELETE CASCADE  GO

ALTER TABLE [dbo].[location] 
ADD 
CONSTRAINT [fk_location_city] FOREIGN KEY ([city_id])
REFERENCES [dbo].[city]([id]) ON DELETE CASCADE  GO

ALTER TABLE [dbo].[location] 
ADD 
CONSTRAINT [fk_location_street] FOREIGN KEY ([street_id])
REFERENCES [dbo].[street]([id]) ON DELETE CASCADE  GO

ALTER TABLE [dbo].[employee]
ADD 
CONSTRAINT [fk_employee_department] FOREIGN KEY ([department_id])
REFERENCES [dbo].[department]([id])ON DELETE CASCADE  GO

ALTER TABLE [dbo].[employee]
ADD 
CONSTRAINT [fk_employee_position] FOREIGN KEY ([position_id])
REFERENCES [dbo].[position]([id]) ON DELETE CASCADE  GO

ALTER TABLE [dbo].[employee]
ADD 
CONSTRAINT [fk_employee_boss] FOREIGN KEY ([boss_id])
REFERENCES [dbo].[employee]([id]) ON DELETE CASCADE  GO

ALTER TABLE [dbo].[employee]
ADD 
CONSTRAINT [fk_employee_identity] FOREIGN KEY ([identity_id])
REFERENCES [dbo].[identity]([id]) ON DELETE CASCADE  GO

ALTER TABLE [dbo].[employee]
ADD 
CONSTRAINT [fk_employee_contact] FOREIGN KEY ([contact_id])
REFERENCES [dbo].[contact]([id]) ON DELETE CASCADE  GO

ALTER TABLE [dbo].[employee]
ADD 
CONSTRAINT [fk_employee_user] FOREIGN KEY ([user_id])
REFERENCES [dbo].[user]([id]) ON DELETE CASCADE  GO

  -- Orest Malanchuk --/ the end / relationship for : [employee] , [location] 

  -- Yura Mazur --/ BEGIN /-------------------------------------------------
ALTER TABLE [dbo].[contact_phone]
ADD 
CONSTRAINT [fk_contact_phone_contact] FOREIGN KEY ([contact_id])
REFERENCES [dbo].[contact]([id]),
CONSTRAINT [fk_contact_phone_phone] FOREIGN KEY ([phone_id])
REFERENCES [dbo].[phone]([id])

ALTER TABLE [dbo].[actor]
ADD
CONSTRAINT [fk_actor_country] FOREIGN KEY ([country_id])
REFERENCES [dbo].[country]([id])

ALTER TABLE [dbo].[client]
ADD
CONSTRAINT [fk_client_staus] FOREIGN KEY ([status_id])
REFERENCES [dbo].[status]([id]),
CONSTRAINT [fk_client_identity] FOREIGN KEY ([identity_id])
REFERENCES [dbo].[identity]([id]),
CONSTRAINT [fk_client_contact] FOREIGN KEY ([contact_id])
REFERENCES [dbo].[contact]([id]),
CONSTRAINT [fk_client_lokation] FOREIGN KEY ([location_id])
REFERENCES [dbo].[location]([id])

ALTER TABLE [dbo].[department]
ADD
CONSTRAINT [fk_department_location] FOREIGN KEY ([location_id])
REFERENCES [dbo].[location]([id])
  -- Yura Mazur --/ END / --------------------------------------------------

  -- Vasylyna Kharkhalis --/ BEGIN /----------------------------------------
ALTER TABLE [dbo].[movie_actor]
ADD 
CONSTRAINT [fk_movie_actor_movie] FOREIGN KEY ([movie_id])
REFERENCES [dbo].[movie]([id]),
CONSTRAINT [fk_movie_actor_actor] FOREIGN KEY ([actor_id])
REFERENCES [dbo].[actor]([id])

ALTER TABLE [dbo].[movie_category]
ADD
CONSTRAINT [fk_movie_category_movie] FOREIGN KEY ([movie_id])
REFERENCES [dbo].[movie]([id]),
CONSTRAINT [fk_movie_category_category] FOREIGN KEY ([category_id])
REFERENCES [dbo].[category]([id])

ALTER TABLE [dbo].[movie_country]
ADD
CONSTRAINT [fk_movie_country_movie] FOREIGN KEY ([movie_id])
REFERENCES [dbo].[movie]([id]),
CONSTRAINT [fk_movie_country_country] FOREIGN KEY ([country_id])
REFERENCES [dbo].[country]([id])

ALTER TABLE [dbo].[movie_producer]
ADD
CONSTRAINT [fk_movie_producer_movie] FOREIGN KEY ([movie_id])
REFERENCES [dbo].[movie]([id]),
CONSTRAINT [fk_movie_producer_producer] FOREIGN KEY ([producer_id])
REFERENCES [dbo].[producer]([id])

ALTER TABLE [dbo].[movie_studio]
ADD
CONSTRAINT [fk_movie_studio_movie] FOREIGN KEY ([movie_id])
REFERENCES [dbo].[movie]([id]),
CONSTRAINT [fk_movie_studio_studio] FOREIGN KEY ([studio_id])
REFERENCES [dbo].[studio]([id])

ALTER TABLE [dbo].[order]
ADD
CONSTRAINT [fk_order_department_id] FOREIGN KEY ([department_id])
REFERENCES [dbo].[department]([id]),
CONSTRAINT [fk_order_order_type_id] FOREIGN KEY ([order_type_id])
REFERENCES [dbo].[order_type]([id]),
CONSTRAINT [fk_order_employee_id] FOREIGN KEY ([employee_id])
REFERENCES [dbo].[employee]([id]),
CONSTRAINT [fk_order_client_id] FOREIGN KEY ([client_id])
REFERENCES [dbo].[client]([id]),
CONSTRAINT [fk_order_currency_id] FOREIGN KEY ([currency_id])
REFERENCES [dbo].[currency]([id]),
CONSTRAINT [fk_order_payment_id] FOREIGN KEY ([payment_id])
REFERENCES [dbo].[payment]([id])

  -- Vasylyna Kharkhalis --/ END / --------------------------------------------------
-------------------------------------------------------------------------------------
  -- Khrystyna Mykich --/ BEGIN /-------------------------------------------------

ALTER TABLE [dbo].[tape_manufacturer]
ADD
CONSTRAINT [FK_412] FOREIGN KEY ([country_id])
REFERENCES [dbo].[country]([id])

ALTER TABLE [dbo].[user]
ADD
CONSTRAINT [FK_610] FOREIGN KEY ([role_id])
REFERENCES [dbo].[user_role]([id])

ALTER TABLE [dbo].[video_tape]
ADD
CONSTRAINT [FK_426] FOREIGN KEY ([type_id])
REFERENCES [dbo].[tape_type]([id]),
CONSTRAINT [FK_430] FOREIGN KEY ([manufacturer_id])
REFERENCES [dbo].[tape_manufacturer]([id]),
CONSTRAINT [FK_434] FOREIGN KEY ([movie_id])
REFERENCES [dbo].[movie]([id])

ALTER TABLE [dbo].[video_tape_order]
ADD
CONSTRAINT [FK_505] FOREIGN KEY ([order_id])
REFERENCES [dbo].[order]([id]),
CONSTRAINT [FK_509] FOREIGN KEY ([video_tape_id])
REFERENCES [dbo].[video_tape]([id])

ALTER TABLE [dbo].[video_type_price]
ADD
CONSTRAINT [FK_476] FOREIGN KEY ([order_type_id])
REFERENCES [dbo].[order_type]([id]),
CONSTRAINT [FK_480] FOREIGN KEY ([department_id])
REFERENCES [dbo].[department]([id]),
CONSTRAINT [FK_484] FOREIGN KEY ([video_tape_id])
REFERENCES [dbo].[video_tape]([id]),
CONSTRAINT [FK_488] FOREIGN KEY ([currency_id])
REFERENCES [dbo].[currency]([id])

  -- Khrystyna Mykich --/ END / --------------------------------------------------
   
-----------------------------------------------------------------------------------------------------------------